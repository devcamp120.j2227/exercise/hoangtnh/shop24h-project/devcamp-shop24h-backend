//Import thư viện express js
const express = require ("express");

//khai báo thư viện mongoose
const mongoose = require("mongoose");

//import thư viện path
const path = require ("path");

//khởi tạo app 
const app = express();

//khai báo cổng chạy app
const port = 8000;

// Cấu hình request đọc được body json
app.use(express.json());

//kết nối với mongo db
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Shop24h",(error) => {
    if(error) throw error;
    console.log("Connect Shop24h to MongoDB successfully!");
})
//khai báo router app
const productTypeRouter = require("./app/routes/productTypeRouter");

//app sử dụng router
app.use("/api", productTypeRouter);

//chạy app trên cổng 8000
app.listen (port, () =>{
    console.log(`App is running on port ${port}`);
})